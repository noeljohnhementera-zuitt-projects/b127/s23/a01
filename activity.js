// Create a singleRoom Document

db.singleRoom.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities"
});

// Add the key-value pair in Singel Room Database
db.singleRoom.updateOne(
    { 
        "_id": ObjectId("613602f4f899af6ae68bde07") 
    },
    {
	$set: {
                "rooms_available": 10
            }
    }
);

// Add the key-value pair in Singel Room Database
db.singleRoom.updateOne(
    { 
        "_id": ObjectId("613602f4f899af6ae68bde07")  
    },
    {
	$set: {
                "isAvailable": false
            }
    }
);


// Create a multipleRooms Document
db.multipleRooms.insertMany([
    	{
         "name": "queen", 
         "accomodates": 4,
         "price": 4000, 
         "description": "A room with a queen sized bed perfect for a simple getaway",
         "rooms_available": 15, 
         "isAvailable": false
         },
         {
         "name": "double", 
         "accomodates": 3,
         "price": 2000, 
         "description": "A room fit for a small family going on a vacation",
         "rooms_available": 5, 
         "isAvailable": false
         }, 	     
]);

// Search for a room with the name double
db.multipleRooms.find({ "name": "double" });

// Update the queen rooom and set the available rooms to 0

db.multipleRooms.updateOne(
    { 
        "_id": ObjectId("613604f6f899af6ae68bde08")  
    },
    {
	$set: {
                "rooms_available": 0
            }
    }
);

// Delete all rooms that have 0 availability

db.multipleRooms.deleteMany({ "rooms_available": 0 });